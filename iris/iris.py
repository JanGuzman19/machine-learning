# Classifies types of iris flowers

import numpy as np # library to deal with big arrays and high level math
from sklearn.datasets import load_iris
from sklearn import tree

iris = load_iris()
test_idx = [0, 50, 100]

#Training data
train_target = np.delete(iris.target, test_idx)
train_data = np.delete(iris.data, test_idx, axis=0)

# Testing data
test_target = iris.target[test_idx]
test_data = iris.data[test_idx]

# Classifier
clfs = tree.DecisionTreeClassifier()
clfs.fit(train_data, train_target)

print(test_target)
print(clfs.predict(test_data))
