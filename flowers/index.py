# Classifies flower images

import tensorflow.contrib.learn as skflow
from sklearn import datasets, metrics

# Fuente de la informacion de los petalos
iris = datasets.load_iris()

# Setting up classifier
classifier = skflow.TensorFlowLinearClassifier( n_classes = 3 )

# Train the classifier
classifier.fit(iris.data, iris.target)

# Grade of accuracy of the guesses
score = metrics.accuracy_score( iris.target, classifier.predict(iris.data) )

print("Accuracy: %f" % score);
