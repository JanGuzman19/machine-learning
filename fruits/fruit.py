# Classification between apples and oranges

from sklearn import tree
# First feature is weight, and 2nd it is wether it's texture is bumpy(0) or smooth(1)
features = [ [140, 1], [130, 1], [150, 0], [170, 0] ]
labels = [0, 0, 1, 1]

#Classifier
clf = tree.DecisionTreeClassifier()

# fit es sinonimo de Find Patterns in Data
clf = clf.fit(features, labels)

# Fruit has 160 grams of weight, and bumpy texture
print(clf.predict([[160, 0]]))

# Output is 0 if it is an apple or 1 if it is an orange
